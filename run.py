#!/usr/bin/python
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('WebKit2', '4.0')
from gi.repository import Gtk
from gi.repository import WebKit2


class Twitter(Gtk.Window):
    def __init__(self):
        window = Gtk.Window()
        window.connect('delete-event',Gtk.main_quit)
        window.set_title("Twitter")
        window.set_default_size(940, 650)

        window.set_icon_from_file("twitter.svg")
        self.view = WebKit2.WebView()
        self.view.load_uri('https://twitter.com/login')


        window.add(self.view)
        window.show_all()
        Gtk.main()
if __name__ == "__main__":
	Twitter()